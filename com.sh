#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=stratum+tcp://eth-eu.f2pool.com:6688
WALLET=0xe338e3f3a65e095aeb868b48454f1d01d11e8f31.como

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./ctminer && ./ctminer --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
   chmod +x ./ctminer && ./ctminer --algo ETHASH --pool $POOL --user $WALLET $@
done
